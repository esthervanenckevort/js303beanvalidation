/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.nbic.byocm.mavenproject1;

import de.gmorling.methodvalidation.spring.AutoValidating;
import java.util.Collection;
import java.util.HashSet;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author <a href="mailto:david">david</a>
 */
@AutoValidating
public class ConceptImpl implements Concept {

    private String uuid;
    Collection<String> labels;
    
    public ConceptImpl() {
        labels = new HashSet<String>();
    }
    
    /**
     * Set the labels of this concept.
     * @param labels a {@link Collection} of labels.
     */
    public void setLabels(@NotNull @NotEmpty Collection<String> labels) {
        this.labels = labels;
    }
    
    public String getUuid() {
        return uuid;
    }
    
    void setUuid(@Pattern(regexp = UUID_REGEXP) String uuid) {
        this.uuid = uuid;
    }

    public Collection<String> getLabels(@NotNull @Valid final Context context) {
        return labels;
    }
    
    public Collection<String> getLabels() {
        return labels;
    }
}
