/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.nbic.byocm.mavenproject1;

import de.gmorling.methodvalidation.spring.AutoValidating;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 *
 * @author <a href="mailto:david">david</a>
 */
@AutoValidating
public interface Service {
    void printConcept(@NotNull @Valid Concept concept);
    
    
    void printLabelsForConcept(@NotNull @Valid Concept concept);
}
