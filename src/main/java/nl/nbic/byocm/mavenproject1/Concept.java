/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.nbic.byocm.mavenproject1;

import de.gmorling.methodvalidation.spring.AutoValidating;
import java.util.Collection;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author <a href="mailto:david">david</a>
 */
@AutoValidating
public interface Concept {
    final static String UUID_REGEXP = "^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$";
    @NotNull
    @Pattern(regexp = UUID_REGEXP)
    String getUuid();
    
    /**
     * Get the labels associated with this concept.
     * @return
     */
    Collection<String> getLabels(@NotNull @Valid Context context);
    @NotNull(groups = { ValidLabels.class })
    @NotEmpty(groups = { ValidLabels.class })
    Collection<String> getLabels();
}
