/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.nbic.byocm.mavenproject1;

import de.gmorling.methodvalidation.spring.AutoValidating;
import java.util.Set;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import org.springframework.stereotype.Component;


/**
 *
 * @author <a href="mailto:david">david</a>
 */
@org.springframework.stereotype.Service("service")
@AutoValidating
public class ServiceImpl implements Service {
    
    @Inject
    private Validator validator;

    public void printConcept(final Concept concept) {
        System.out.println(concept.getUuid());
    }

    public void printLabelsForConcept(Concept concept) {
        Set<ConstraintViolation<Concept>> result = validator.validate(concept, ValidLabels.class);
        if (!result.isEmpty()) {
            throw new ConstraintViolationException((Set)result);
        }
        for (String label : concept.getLabels(new ContextImpl())) {
            System.out.println(label);
        }
    }
}
