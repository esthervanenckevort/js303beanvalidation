/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.nbic.byocm.mavenproject1;

import java.util.Collection;

/**
 *
 * @author <a href="mailto:david.van.enckevort@nbic.nl">David van Enckevort</a>
 */
public interface Context {
    Collection<Language> getLanguages();
    Collection<Branch> getBranches();
    
}
