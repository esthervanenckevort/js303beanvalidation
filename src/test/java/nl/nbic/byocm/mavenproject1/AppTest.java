package nl.nbic.byocm.mavenproject1;

import java.util.UUID;
import javax.inject.Inject;
import javax.validation.ConstraintViolationException;
import org.hibernate.validator.method.MethodConstraintViolationException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Unit test for simple App.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"/nl/nbic/byocm/mavenproject1/newSpringXMLConfig.xml"})
public class AppTest {

    @Inject
    private Service service;

    @Test(expected = MethodConstraintViolationException.class)
    public void testNull() {
        service.printConcept(null);
    }

    @Test(expected = MethodConstraintViolationException.class)
    public void testNullUuid() {
        Concept concept = new ConceptImpl();
        service.printConcept(concept);
    }
    
    @Test(expected = MethodConstraintViolationException.class)
    public void testInvalidUuid() {
        ConceptImpl concept = new ConceptImpl();
        concept.setUuid("bla");
        service.printConcept(concept);
    }
    
    @Test
    public void testValidUuid() {
        ConceptImpl concept = new ConceptImpl();
        concept.setUuid(UUID.randomUUID().toString());
        service.printConcept(concept);
    }
    
    @Test(expected = ConstraintViolationException.class)
    public void testPrintLabelsWithEmptyLabels() {
        ConceptImpl concept = new ConceptImpl();
        concept.setUuid(UUID.randomUUID().toString());
        service.printLabelsForConcept(concept);
    }
}
